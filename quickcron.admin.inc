<?php

/**
 * @file
 * Quickcron admin pages and quickcron launch functions.
 */

/**
 * Quickcron admin page form.
 */
function quickcron_admin_page_form($form, &$form_state) {
  $quickcron_status = !lock_may_be_available('quickcron');
  if ($quickcron_status) {
    $form['status'] = array(
      '#type' => 'item',
      '#title' => t('Status'),
      '#markup' => t('Quickcron is running'),
    );
    $form['stop'] = array(
      '#type' => 'submit',
      '#value' => t('Stop'),
    );
    $form['restart'] = array(
      '#type' => 'submit',
      '#value' => t('Restart'),
    );
    if (isset($form_state['storage']['clear_locks'])) {
      $form['clear'] = array(
        '#type' => 'submit',
        '#value' => 'Clear locks',
      );
    }
  }
  else {
    $form['status'] = array(
      '#type' => 'item',
      '#title' => t('Status'),
      '#markup' => t('Quickcron is NOT running'),
    );
    $form['start'] = array(
      '#type' => 'submit',
      '#value' => t('Start'),
    );
  }
  $form['#submit'] = array('quickcron_admin_page_form_submit');
  return $form;
}

/**
 * Quickcron config page form.
 */
function quickcron_config_page_form($form, &$form_state) {
  $form += array(
    'quickcron_run_time' => array(
      '#type' => 'textfield',
      '#title' => t('Execution time limit'),
      '#description' => t('Quickcron script max execution time, seconds.'),
      '#default_value' => variable_get('quickcron_run_time', '630'),
    ),
    'quickcron_period' => array(
      '#type' => 'textfield',
      '#title' => t('Task execution period'),
      '#description' => t('A period of time hook_quickcron must be called, seconds.'),
      '#default_value' => variable_get('quickcron_period', '1'),
    ),
    'quickcron_key' => array(
      '#type' => 'textfield',
      '#title' => t('Quickcron key'),
      '#description' => t("Quickron can be protected from external run by key, leave empty for using no key. Otherwise, you must specify 'quickron_key' GET argument in your query (i.e. 'quickcron.php?quickron_key=[your key]')."),
      '#default_value' => variable_get('quickcron_key', ''),
    ),
    'quickcron_log' => array(
      '#type' => 'checkbox',
      '#title' => t('Log quickcron startup and shutdown'),
      '#default_value' => variable_get('quickcron_log', 1),
    ),
  );
  return system_settings_form($form);
}

/**
 * Stop quickcron handler.
 */
function quickcron_stop(&$form_state) {
  // Set lock.
  if (!lock_acquire('new_quickcron', 5)) {
    drupal_set_message(t('Can\'t acquire lock "new_quickcron", try again later.'), 'warning');
    return;
  }
  else {
    // Wait for quickcron shutdown.
    $max_wait_time = 10;
    $time = 0;
    while (!lock_may_be_available('quickcron')) {
      sleep(1);
      $time++;
      if ($time >= $max_wait_time) {
        drupal_set_message(t('Quickcron does not want to shut down. If you are certain that quickcron is not running you may use "clear locks" button.'), 'error');
        $form_state['storage']['clear_locks'] = TRUE;
        $form_state['rebuild'] = TRUE;
        lock_release('new_quickcron');
        return;
      }
    }
    drupal_set_message(t('Quickcron was stopped.'));
    lock_release('new_quickcron');
    return;
  }
}

/**
 * Start quickcron handler.
 */
function quickcron_start() {
  if (!lock_may_be_available('quickcron')) {
    drupal_set_message(t('Quickcron is already running.'));
    return;
  }
  $quickcron_key = variable_get('quickcron_key', '');
  $script_url = url(drupal_get_path('module', 'quickcron') . '/quickcron.php', array('absolute' => TRUE, 'query' => ($quickcron_key ? array('quickcron_key' => $quickcron_key) : array())));
  $str = exec('wget ' . $script_url . ' -O /tmp/quickcron-buf > /dev/null &');

  // Sleep to make sure quickcron.php started and lock acquired.
  sleep(1);

  drupal_set_message(t('Quickcron started.'));
}

/**
 * Admin page form submit handler.
 */
function quickcron_admin_page_form_submit($form, &$form_state) {
  if (isset($form['start']) && $form_state['clicked_button']['#id'] == $form['start']['#id']) {
    quickcron_start();
  }
  elseif (isset($form['stop']) && $form_state['clicked_button']['#id'] == $form['stop']['#id']) {
    quickcron_stop($form_state);
  }
  elseif (isset($form['restart']) && $form_state['clicked_button']['#id'] == $form['restart']['#id']) {
    quickcron_stop($form_state);
    quickcron_start();
  }
  elseif (isset($form['clear']) && $form_state['clicked_button']['#id'] == $form['clear']['#id'] && isset($form_state['storage']['clear_locks'])) {
    global $locks;
    unset($form_state['storage']['clear_locks']);
    // Clearing locks.
    foreach (array('quickcron', 'new_quickcron') as $lock_name) {
      unset($locks[$lock_name]);
      db_delete('semaphore')
        ->condition('name', $lock_name)
        ->execute();
    }
    drupal_set_message(t('Quickcron locks cleared.'));
  }
}

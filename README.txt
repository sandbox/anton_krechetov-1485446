Description
-----------
Quickcron is similar to Drupal cron but it's designed to run tasks that should
be executed very frequently, like once a second.

Quickcron tasks are handled by quickcron.php script which resides in module
folder. When quickcron.php is executed, it fires off all quickcron tasks every
predefined amount of time (which can be configured) and sleeps in between. The
execution is finished when another instance of quickcron.php starts or script
execution reaches time limit.

To integrate your module with quickcron, implement hook_quickcron() which has
no parameters and return value. Don't write any long-running tasks, quickcron
is not designed for them.


Installation
------------
Install as usual, see http://drupal.org/node/70151 for details.
There are two ways to launch quickcron:
  a. (recommended) Set up independent cron daemon job to execute
     quickcron.php. More information about cron maintenance
     tasks is available at http://drupal.org/cron.
  b. Run quickcron on the Administration » Configuration » System » Quickcron.
     This is a one-time launch, the quickcron won't restart after it's
     finished.


Configuration
-------------
- Go to Administration » Configuration » System » Quickcron » Settings to set
  up quickcron.
-- "Execution time limit" setting should be slightly more than cron daemon job
   period. For example, if time period for crontab job equals to 600 seconds,
   then a suitable "Execution time limit" setting will be 630 seconds.
-- "Task execution period" defines how frequently quickcron tasks are fired.
   Set this value to slightly more than a maximum time you expect all quickcron
   tasks will take. Note that when their execution exceeds this time, an error
   is logged to watchdog.
-- You can protect quickcron with a secret key, so it'll be impossible to
   launch quickcron script without it. Enter your key into the "Quickcron
   key" field and add 'quickron_key' GET argument to your crontab rule.

<?php

/**
 * @file
 * Fires off quickcron jobs every second or so.
 * This file is script meant to be run from cron daemon.
 */

$drupal_path = quickcron_drupal_path();
if (empty($drupal_path)) {
  exit;
}

chdir($drupal_path);
define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Quickcron can be protected by key.
$quickcron_key = variable_get('quickcron_key', '');
if ($quickcron_key && (!isset($_GET['quickcron_key']) || $quickcron_key != $_GET['quickcron_key'])) {
  watchdog('quickcron', 'Quickcron could not run because an invalid key was used.', array(), WATCHDOG_NOTICE);
  drupal_access_denied();
}
elseif (variable_get('maintenance_mode', 0)) {
  watchdog('quickcron', 'Quickcron could not run because the site is in maintenance mode.', array(), WATCHDOG_NOTICE);
  drupal_access_denied();
}
else {
  quickcron_run_quikcron();
}

/**
 * Detects Drupal path by scanning each level of current path.
 */
function quickcron_drupal_path() {
  $bootstrap = '/includes/bootstrap.inc';
  $path = dirname(str_replace('\\', '/', __FILE__));
  while (($pos = strrpos($path, '/')) !== FALSE) {
    $path = substr($path, 0, $pos);
    if (@file_exists($path . $bootstrap)) {
      return $path;
    }
  }
  return NULL;
}
